json.array!(@people) do |person|
  json.extract! person, :id, :nome, :email
  json.url person_url(person, format: :json)
end
